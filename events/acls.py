from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state + " " + "street",
        "size": "medium",
        "locale": "en-US",
    }
    # Depends on API, you may use the "," or other seperators

    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["medium"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
        # Can we just use except with empty condition? Not great practice. PEP8, may take all errors.


def get_weather(city, state):
    params = {
        "q": city + "," + state + "," + "US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    geo_url = "http://api.openweathermapp.org/geo/1.0/direct"
    response = requests.get(geo_url, params=params)
    geo_content = json.loads(response.content)

    #
    # geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    # geo_content = requests.get(url_geo)
    # lat = geo_content[0]["lat"]
    # lon = geo_content[0]["lon"]

    try:
        lat = geo_content[0]["lat"]
        lon = geo_content[0]["lon"]
    except (KeyError, IndexError):
        return None
        # Is this None? Other options?

    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(weather_url, weather_params)
    weather_content = json.loads(response.content)
    weather = {
        "temp": weather_content["main"]["temp"],
        "description": weather_content["weather"][0]["description"],
    }

    # url_weather = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # weather_content = requests.get(url_weather)
    # weather = {
    #     "temp": weather_content["main"]["temp"],
    #     "description": weather_content["weather"][0]["description"],
    # }

    # Wrong: We don't need extra dictionary
    # try:
    #     return {"weather": weather}
    # except (KeyError, IndexError):
    #     return {"weather": None}

    try:
        return weather
    except (KeyError, IndexError):
        return None
